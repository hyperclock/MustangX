# MustangX

is a Multilingual Content Management System. Built as a full scale blog, but with some sort of expansion system, we will have a CMS as is needed. We start off with utilizing the <a target="_blank" rel="noopener noreferrer" href="https://symfony.com">Symfony4 Framework</a> and <a target="_blank" rel="noopener noreferrer" href="https://getcomposer.com">Composer</a>.

## Features
These are features already implemented. This will be updated as features get added to the core system.

  *  Multilanguage
      *  English - default
      *  German - secondary as demo
  * Contact Form
  * Twitter Bootstrap CDN
  * FontAwesome CDN


## Planned Features
These are things that are features I would like to have, so I'm considering these as planned features.

  *  Translation method for the content
  *  EU Cookie Compliance
  *  Legal info page examples
  *  Blog or News pages
  *  Static Pages
  *  Captcha
  *  Themes or Skins
  *  Modules, extensions, plugins
  *  Admin Area
  *  User stuff
  *  Method to install update, theme, modules in the backend


## Documentation
Will have development docs as I develop in the [WIKI](wiki).

Other docs as soon as is usable.


## License
<i aria-hidden="true" data-hidden="true" class="fa fa-balance-scale fa-fw"></i>
This project is licensed under the ***BSD 3-Clause License***, see [LICENSE](LICENSE) for details.

-----
-----

## Get started from GIT

#### Get the code
```bash
git clone https://gitlab.com/hyperclock/MustangX.git MustangX
```

#### Update the code
```bash
cd MustangX

composer update
```



#### Make sure you have a DB

Copy the *.env* file and rename it to *.env.local*
```bash
cp .env .env.local
```

Add your database credentials to the *.env.local* file

and then do this (on the commandline) to CREATE a database:
```bash
php bin/console doctrine:database:create
```
***IF*** you allready have a database, add your credentials and do this:
```bash
php bin/console doctrine:database:update
```

#### Use the dev server

The full Symfony Framework has it's own dev server and so let's use it.
```bash
symfony server:start
```
You can turn it off with `ctrl+c`

#### Get the assets updated
```bash
php bin/console assets:install --symlink --relative public
```

#### Update everything
```bash
composer update --with-dependencies
```
