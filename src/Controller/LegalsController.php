<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class LegalsController extends AbstractController
{
    /**
     * @Route("/terms", name="terms")
     */
    public function index(): Response
    {
        return $this->render('legals/terms.html.twig');
    }

    /**
     * @Route("/disclaimer", name="disclaimer")
     */
    public function disclaimer(): Response
    {
        return $this->render('legals/disclaimer.html.twig');
    }

    /**
     * @Route("/privacy-policy", name="privacy-policy")
     */
    public function privacypolicy(): Response
    {
        return $this->render('legals/privacy-policy.html.twig');
    }

    /**
     * @Route("/imprint", name="imprint")
     */
    public function imprint(): Response
    {
        return $this->render('legals/imprint.html.twig');
    }

    /**
     * @Route("/trademarks", name="trademarks")
     */
    public function trademarks(): Response
    {
        return $this->render('legals/trademarks.html.twig');
    }
}
